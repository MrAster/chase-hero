#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

class Entity // Entity class
{
private:
    sf::Vector2i Signs;
    sf::Vector2f offset;

public:  // Everything public because screw gravity

    // These are the possible entity types
    enum EntityTypes
    {
        player,
        enemy,
        bullet
    };

    sf::RectangleShape Body; //Every entity is a rectangle, it's that simple.
    int8_t Health;           // Every entity has health
    int8_t Type;             // Every entity can have three types: player, enemy, bullet
    sf::Vector2f Position2;  // Miscellaneous vector

    Entity(int8_t Type_, sf::Vector2f Position1, sf::Vector2f Position2_); // Default ctr, asks for type, position, and miscellaneous vector

    void Update(); // Entity update function

};
