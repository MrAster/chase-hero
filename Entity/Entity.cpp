#include "Entity.hpp"

Entity::Entity(int8_t Type_, sf::Vector2f Position1, sf::Vector2f Position2_) // Default ctr, asks for type, position, and miscellaneous vector
{
    //assign type
    Type = Type_;

    // assign miscellaneous vector
    Position2 = Position2_;

    Body.setPosition(Position1); // set position

    switch(Type)        // Switch on entity type
    {
    case EntityTypes::player:                   // If player, create player entity
        Body.setFillColor(sf::Color::Cyan);
        Body.setOutlineColor(sf::Color::Black);
        Body.setOutlineThickness(5);
        Body.setPosition(Position1);
        Body.setSize(sf::Vector2f {30,30});
        Body.setOrigin(15.f,15.f);
        break;

    case EntityTypes::bullet:                    // If bullet, create bullet entity
        Body.setFillColor(sf::Color::Yellow);
        Body.setOutlineColor(sf::Color::White);
        Body.setOutlineThickness(1);
        Body.setPosition(Position1);
        Body.setSize(sf::Vector2f {9,3});
        Body.setOrigin(5.f,2.f);
        break;

    case EntityTypes::enemy:                    // if enemy, create enemy entity
        Body.setFillColor(sf::Color::Red);
        Body.setOutlineColor(sf::Color::Blue);
        Body.setOutlineThickness(4);
        Body.setPosition(Position1);
        Body.setSize(sf::Vector2f {25,25});
        Body.setOrigin(14.5f,14.5f);
        break;

    default:
        break;

    }
}

void Entity::Update()                       // Entity update function
{
    switch(Type)
    {
    case EntityTypes::bullet:     // If bullet, move at Position2 pixels per call
        Body.move(Position2);
        break;

    case EntityTypes::enemy:     // If enemy, move towards player
        if(((fabs(fabs(Body.getPosition().x) - fabs(Position2.x))) || fabs(fabs(Body.getPosition().y) - fabs(Position2.y))) <= 100)
        {
            offset = sf::Vector2f(Body.getPosition().x - Position2.x, Body.getPosition().y - Position2.y);

            if(offset.x > 0)  // This and the code above helps obtain the direction in which the bullet will travel. The Signs vector is used because we're using absolute values below
                Signs.x = -1;
            else
                Signs.x = 1;

            if(offset.y > 0)
                Signs.y = -1;
            else
                Signs.y = 1;


            if(abs(offset.y) > abs(offset.x))
            {
                offset.x = fabs(offset.x / offset.y);
                offset.y = 1;
            }
            else
            {
                offset.y = fabs(offset.y / offset.x);
                offset.x = 1;
            }

            offset.x *= Signs.x * 3; // Speed up by 7, twice the speed of the player.
            offset.y *= Signs.y * 3;

            Body.setPosition(Body.getPosition().x + offset.x, Body.getPosition().y + offset.y);
        }
        break;

    default:
        break;
    }
}
