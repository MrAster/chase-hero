#include "TilingEngine.hpp"

void TilingEngine::setPosition(sf::Vector2f position) // set the position on which to render around, this is unused for now
{
    currentPosition = position;
}

void TilingEngine::setPosition(float x, float y)      // see above
{
    currentPosition = sf::Vector2f {x,y};
}

void TilingEngine::setTexture(std::string filename) // Set the texture to be rendered
{
    mainTexture.loadFromFile(filename);
}

void TilingEngine::setTexture(sf::Texture &texture) // see above
{
    mainTexture = texture;
}

void TilingEngine::Render(sf::RenderWindow &window, sf::Vector2f Position) // This is the render function, args are a window and a position to render around
{
    //potatoes;

    sf::Vector2f spritePosition; // This is the position on which the sprite shall be rendered

    for(int py {-1}; py <= 1; py++) // Render three times the size of the screen around the player on the y coord
    {
        for(int px {-1}; px <= 1; px++) // Render three times the size of the screen around the player on the x coord
        {
            for(float y {0}; y < window.getSize().y / (mainSprite.getLocalBounds().height * mainSprite.getScale().y); y++) // Render tiles along the y axis
            {
                for(float x {0}; x < window.getSize().x / (mainSprite.getLocalBounds().width * mainSprite.getScale().x); x++) // Render tiles along the x axis
                {


                    // Due to C++'s integer truncating. we need to subtract 1 from the sprite's position if the position to render around is negative.
                    // I have split this into two if statements because if I just check if one of the two positions on the axis is negative, it renders wrong.

                    if(Position.x > 0)
                    {
                        // multiply the sprite's width by the position on the tilemap, then multiply by the sprite's scale. After that, add the position offset, which is calculated by getting the position in units of screen size
                        spritePosition.x = (mainSprite.getLocalBounds().width  * x * mainSprite.getScale().x + ( (px + (int)((int)Position.x / (int)window.getSize().x)) * (int)window.getSize().x ));
                    }
                    else
                    {
                        // multiply the sprite's width by the position on the tilemap, then multiply by the sprite's scale. After that, add the position offset, which is calculated by getting the position in units of screen size.
                        // Subtract 1 from this number because render position is negative.
                        spritePosition.x = (mainSprite.getLocalBounds().width  * x * mainSprite.getScale().x + ( (px - 1 + (int)((int)Position.x / (int)window.getSize().x)) * (int)window.getSize().x ));
                    }

                    if(Position.y > 0)
                    {
                        // multiply the sprite's height by the position on the tilemap, then multiply by the sprite's scale. After that, add the position offset, which is calculated by getting the position in units of screen size
                        spritePosition.y = (mainSprite.getLocalBounds().height * y * mainSprite.getScale().y + ( (py + (int)((int)Position.y / (int)window.getSize().y)) * (int)window.getSize().y ));
                    }
                    else
                    {
                        // multiply the sprite's width by the position on the tilemap, then multiply by the sprite's scale. After that, add the position offset, which is calculated by getting the position in units of screen size.
                        // Subtract 1 from this number because render position is negative.
                        spritePosition.y = (mainSprite.getLocalBounds().height * y * mainSprite.getScale().y + ( (py - 1 + (int)((int)Position.y / (int)window.getSize().y)) * (int)window.getSize().y ));
                    }

                    // Finally apply the position.
                    mainSprite.setPosition(spritePosition);

                    // Draw the sprite to the window given
                    window.draw(mainSprite);
                }
            }
        }
    }
}


TilingEngine::TilingEngine(std::string filename, sf::Vector2i windowSize_) // base constructor
{
    // load the file
    if(mainTexture.loadFromFile(filename))
    {
        // assign a useless variable
        windowSize = windowSize_;
    }

    //Set the sprite's source texture to the one we just loaded
    mainSprite.setTexture(mainTexture);
}
