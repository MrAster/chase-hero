#pragma once
#include <SFML/Graphics.hpp>
#include <cmath>

#ifdef _WIN32
#define M_PI 3.14159
#endif

class TilingEngine // Tiling engine class; only supports one texture, that is tiled around a given position in a 3-screen-radius
{
private:
    sf::Texture mainTexture; // This is the texture we'll be tiling...
    sf::Sprite mainSprite;   // However, to use scaling effects and whatnot, we need a sprite, so we'll use a sprite, too
    sf::Vector2i windowSize; // This is unused, really. It'll be used in the future
    sf::Vector2f currentPosition; // The position on which to render around

public:

    void setPosition(sf::Vector2f position); // set the position on which to render around, this is unused for now

    void setPosition(float x, float y);      // see above

    void setTexture(std::string filename); // Set the texture to be rendered

    void setTexture(sf::Texture &texture); // see above

    void Render(sf::RenderWindow &window, sf::Vector2f Position); // This is the render function, args are a window and a position to render around

    TilingEngine(std::string filename, sf::Vector2i windowSize_ = sf::Vector2i {0,0}); // base constructor
};
