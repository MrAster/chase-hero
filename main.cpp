#include <iostream>
#include <cmath>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <memory>
#include "TilingEngine/TilingEngine.hpp"
#include "Entity/Entity.hpp"

int main(int argc, char **argv)
{
    // initialize tiling engine.
    TilingEngine Tyler {"image2.png"};

    bool gamePaused, gameOver; // booleans used for winning/losing conditions

    sf::Clock mainTimer;

    int64_t timerLoopsCounter {1}; // Helps to regulate the frequency at which the physics stuff is called
	
    // This struct is used to regulate the speed at which you can click, spamming physics entities isn't on my checklist
    struct
    {
        int32_t Right,Left;
    } mouseClickTracker;


    //initialize struct variables
    mouseClickTracker.Right = 0;
    mouseClickTracker.Left = 0;

    sf::RenderWindow mainWindow {sf::VideoMode{480,320}, "Chase Hero!!"}; //Game window blah

    //Create the view of the game
    sf::View mainView;

    //Set the size of the view to the size of the window
    mainView.reset(sf::FloatRect {0.f, 0.f, (float)mainWindow.getSize().x, (float)mainWindow.getSize().y});

    //set the view's viewport to the full window
    mainView.setViewport(sf::FloatRect {0.f, 0.f, 1.f, 1.f});


    //load the game's font. Arial.ttf in this case.
    sf::Font mainFont;
    mainFont.loadFromFile("Arial.ttf");

	int64_t frameTime {0};
	sf::Text textOverlay {"Current FPS: ", mainFont, 12};
	textOverlay.setPosition(5,5);
	
	
    //prepare the text spawned on losing condition
    sf::Text gameOverText {"GAME OVER", mainFont, 100};
    gameOverText.setOrigin(gameOverText.getGlobalBounds().width/2, gameOverText.getGlobalBounds().height/2);
    gameOverText.setPosition(mainWindow.getSize().x/2, mainWindow.getSize().y/2);



    // vector in which our entities will be kept
    std::vector<Entity> Entities;

    // Entities[0] will always be the player. I'm a genius.
    Entities.push_back(Entity {Entity::EntityTypes::player, sf::Vector2f{mainWindow.getSize().x/2.f - 15.f, mainWindow.getSize().y/2.f - 15.f}, sf::Vector2f{0.f,0.f}});

    mainTimer.restart();

    gamePaused = false;
    gameOver   = false;

    while(mainWindow.isOpen()) //Game loop
    {
        sf::Event mainEvent;
        while(mainWindow.pollEvent(mainEvent))
        {
            switch(mainEvent.type)
            {

            case sf::Event::Closed:
                mainWindow.close();
                break;

            case sf::Event::Resized:
                mainView.setSize(sf::Vector2f(mainEvent.size.width, mainEvent.size.height));
                mainWindow.setView(mainView);
                break;

            case sf::Event::GainedFocus:
                //If the window gains focus, we'll unpause the game
                if(!gameOver)
                {
                    gamePaused = false;
                    std::cout<<"GAME UNPAUSED."<<std::endl;
                }
                break;

            case sf::Event::LostFocus:
                //If the window loses focus, we'll pause the game
                if(!gameOver)
                {
                    gamePaused = true;
                    std::cout<<"GAME PAUSED."<<std::endl;
                }
                break;

            case sf::Event::MouseWheelMoved:
                //The mouse wheel will zoom the view, used for testing tiling engine
                if(mainEvent.mouseWheel.delta > 0)
                {
                    mainView.zoom(2);
                }
                else
                {
                    mainView.zoom(0.5);
                }
                break;

            default:
                break;
            }
        }

        if(mainTimer.getElapsedTime().asMilliseconds() > timerLoopsCounter) // All the controls stuff
        {
            timerLoopsCounter = mainTimer.getElapsedTime().asMilliseconds() + 14; // Regulate calls to 16ms/call minimum

            mainWindow.setView(mainView);

            mainWindow.clear(sf::Color::Green);

            if(!gamePaused)
            {
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W)) // Keyboard input, nothing interesting
                {
                    Entities[0].Body.move( 0,-4);
                }

                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A)) // Ditto, see above
                {
                    Entities[0].Body.move(-4, 0);
                }

                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) // Ditto, see above
                {
                    Entities[0].Body.move( 0, 4);
                }

                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) // Ditto, see above
                {
                    Entities[0].Body.move( 4, 0);
                }

                if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && mainTimer.getElapsedTime().asMilliseconds() > mouseClickTracker.Left) // Mouseclick = fire. this is more interesting.
                {
                    mouseClickTracker.Left = mainTimer.getElapsedTime().asMilliseconds() + 250; // Make it so that you can only fire once every 250 ms

                    sf::Vector2f offset(sf::Mouse::getPosition(mainWindow) - (sf::Vector2i) mainWindow.getSize()/2);
                    sf::Vector2f Signs;

                    if(offset.x > 0)  // This and the code above helps obtain the direction in which the bullet will travel. The Signs vector is used because we're using absolute values below
                        Signs.x = 1;
                    else
                        Signs.x = -1;

                    if(offset.y > 0)
                        Signs.y = 1;
                    else
                        Signs.y = -1;


                    if(abs(offset.y) > abs(offset.x))
                    {
                        offset.x = fabs(offset.x / offset.y);
                        offset.y = 1;
                    }
                    else
                    {
                        offset.y = fabs(offset.y / offset.x);
                        offset.x = 1;
                    }

                    offset.x *= Signs.x * 7; // Speed up by 7, twice the speed of the player.
                    offset.y *= Signs.y * 7;

                    Entities.push_back(Entity {Entity::EntityTypes::bullet, Entities[0].Body.getPosition(), offset}); // Finally create bullet entity

                    Entities[Entities.size()-1].Body.rotate(std::atan(offset.y / offset.x) * 180 / M_PI); // Rotate Bullet entity so that it is parallel to trajectory.

                }

                // If the right mouse button is clicked, spawn an enemy
                if(sf::Mouse::isButtonPressed(sf::Mouse::Right) && mainTimer.getElapsedTime().asMilliseconds() > mouseClickTracker.Right)
                {
                    mouseClickTracker.Right = mainTimer.getElapsedTime().asMilliseconds() + 150; // Make it so that you can only place an enemy every 150 ms
                    sf::Vector2f offset {sf::Mouse::getPosition(mainWindow).x + Entities[0].Body.getPosition().x - mainWindow.getSize().x/2, sf::Mouse::getPosition(mainWindow).y + Entities[0].Body.getPosition().y - mainWindow.getSize().y/2};

                    Entities.push_back(Entity {Entity::EntityTypes::enemy, offset, Entities[0].Body.getPosition()});
                }


                // Set the view's center to the player's position
                mainView.setCenter(Entities[0].Body.getPosition());

                Tyler.Render(mainWindow, Entities[0].Body.getPosition());


                for(unsigned int i {0}; i < Entities.size(); i++) // Entity update loop
                {

                    //This little if statement updates the enemy's target, so that it is always following the player
                    if(Entities[i].Type == Entity::EntityTypes::enemy)
                    {
                        Entities[i].Position2 = Entities[0].Body.getPosition();
                    }

                    // This updates the entity physics stuff
                    Entities[i].Update();

                    mainWindow.draw(Entities[i].Body);


                    // This loop checks for whether an enemy and a bullet collide. If they do, we remove the enemy and the bullet
                    // However, if an enemy and the player collide, then the game is over
                    for(unsigned int y {0}; y < Entities.size(); y++)
                    {
                        if(Entities[i].Body.getGlobalBounds().intersects(Entities[y].Body.getGlobalBounds()))
                        {
                            if(((Entities[i].Type == Entity::EntityTypes::enemy) && (Entities[y].Type == Entity::EntityTypes::bullet))
                                    || ((Entities[i].Type == Entity::EntityTypes::bullet) && (Entities[y].Type == Entity::EntityTypes::enemy)))
                            {
                                Entities.erase(Entities.begin()+i);
                                Entities.erase(Entities.begin()+y);
                            }

                            if(((Entities[i].Type == Entity::EntityTypes::enemy) && (Entities[y].Type == Entity::EntityTypes::player))
                                    || ((Entities[i].Type == Entity::EntityTypes::player) && (Entities[y].Type == Entity::EntityTypes::enemy)))
                            {
                                gameOverText.setPosition(Entities[0].Body.getPosition());
                                gameOver = true;
                                gamePaused = true;
                            }
                        }
                    }
                }

                mainWindow.draw(Entities[0].Body);

                if(gameOver)
                    mainWindow.draw(gameOverText);

				textOverlay.setPosition(Entities[0].Body.getPosition().x - mainWindow.getSize().x/2 + 5,
										Entities[0].Body.getPosition().y - mainWindow.getSize().y/2 + 5);
										
				textOverlay.setString(std::string{"Chase Hero!! Alpha v0.0.2 DND\n"}
									+ std::string{ "Current FPS: " + std::to_string( 1000 / (mainTimer.getElapsedTime().asMilliseconds() - frameTime)) + "\n"}
									+ std::string{ "Current amount of entities: " + std::to_string(Entities.size()) + "\n"});
												
				frameTime = mainTimer.getElapsedTime().asMilliseconds();
				mainWindow.draw(textOverlay);
                mainWindow.display(); // Display drawn stuff
				
            }
        }

    }
}
